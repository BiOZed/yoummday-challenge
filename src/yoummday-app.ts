import { LitElement, html, css } from 'lit'
import { customElement, property } from 'lit/decorators.js'

@customElement('yoummday-app')
export class YoummdayApp extends LitElement {
	@property({ type: Number })
	activeIndex = 0
	
	@property({ type: String })
	alignment = 'left'
	
	@property({ type: String })
	activeItemsJSON = ''
	
	@property({ type: Object })
	data = {
		pageTitle: 'Bonjour',
		dropdownLabel: 'Dropdown element',
		sets: [
			[
				{
					label: 'Action 1',
					href: '#1',
					active: 1
				},
				{
					label: 'Action 2',
					href: '#2'
				},
				{
					label: 'Action 3',
					href: '#3'
				},
			],
			[
				{
					label: 'Action 4',
					href: '#4',
					active: 1
				},
				{
					label: 'Action 5',
					href: '#5'
				},
				{
					label: 'Action 6',
					href: '#6',
					active: 1
				},
			]
		]
	}

	render() {
		return html`
<h1>${this.data.pageTitle}</h1>
<yoummday-dropdown
toggleLabel=${this.data.dropdownLabel}
alignTooltip=${this.alignment}
items=${this.activeItemsJSON}
>
</yoummday-dropdown>
<div style="margin-top: 30px">
<button @click="${this._swapData}">Swap dataset</button>
<button @click="${this._swapAlignment}">Toggle left/right alignment</button>
</div>
`
	}

	connectedCallback() {
		super.connectedCallback()
		this.activeItemsJSON = JSON.stringify(this.data.sets[this.activeIndex])
	}

	_swapData() {
		this.activeIndex = this.activeIndex === 0 ? 1 : 0
		this.activeItemsJSON = JSON.stringify(this.data.sets[this.activeIndex])
	}
	
	_swapAlignment() {
		this.alignment = this.alignment === 'left' ? 'right' : 'left'
	}

	
	static styles = css`
:host {
margin: 0 auto;
max-width: 1280px;
text-align: center;
}
`
}

declare global {
	interface HTMLElementTagNameMap {
		'yoummday-app': YoummdayApp
	}
}
