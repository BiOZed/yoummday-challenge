import { LitElement, css, html } from 'lit'
import { customElement, property } from 'lit/decorators.js'
import { createPopper } from '@popperjs/core'

@customElement('yoummday-dropdown')
export class YoummdayDropdown extends LitElement {
	@property({ type: String })
	toggleLabel = ''

	@property({ type: String })
	alignTooltip = 'left'

	@property({ type: Array })
	items = []

	render() {
		this._formatItems()
		return html`
<div class="dropdown">
<button class="dropdown-toggle">${this.toggleLabel}</button>
<ul class="dropdown-tooltip">
${this.items.map((item) =>
html`<li class="dropdown-item"><a class="dropdown-link ${item.activeClass}" href="${item.href}">${item.label}</a></li>`
)}
</ul>
</div>
`
	}

	firstUpdated() {
		this._getElements()
		this._bindEvents()
	}

	updated() {
		this._getElements()
		
		const placement = this.alignTooltip === 'right' ? 'bottom-end' : 'bottom-start'
		createPopper(this.toggle, this.tooltip, {
			placement: placement
		})
	}

	_getElements() {
		this.root = this.renderRoot.querySelector('.dropdown')
		this.toggle = this.renderRoot.querySelector('.dropdown-toggle')
		this.tooltip = this.renderRoot.querySelector('.dropdown-tooltip')
	}

	_formatItems() {
		this.items.map((item) => {
			if (item.active) {
				item.activeClass = 'dropdown-link--active'
			}
		})
	}

	_bindEvents() {
		this.toggle.addEventListener('click', () => { this._toggleVisibility() } )
		// improve: this event pollutes the global scope and should be removed when the component is disconnected (I am used to namescoped events with jQuery and would need to figure out the best alternative)
		window.document.addEventListener('click', (e) => { this._handleDocumentClick(e) })
	}

	_handleDocumentClick (e) {
		if (e.target !== this.root && e.originalTarget.closest('.dropdown') !== this.root ) {
			this._hide()
		}
	}

	_toggleVisibility() {
		if (this._isVisible()) {
			this._hide()
		} else {
			this._show()
		}
	}

	_show() {
		this.tooltip.classList.add('dropdown-tooltip--visible')
	}

	_hide() {
		this.tooltip.classList.remove('dropdown-tooltip--visible')
	}

	_isVisible() {
		return this.tooltip.classList.contains('dropdown-tooltip--visible')
	}

	static styles = css`
:host {
margin: 0 auto;
max-width: 1280px;
text-align: center;
}

.dropdown-toggle {
font-size: 1.25em;
}

.dropdown-tooltip {
background: gray;;
border: 1px solid black;
border-radius: 5px;
display: none;
padding: 8px 0;
list-style: none;
text-align: left;
}

.dropdown-tooltip--visible {
display: block;
}

.dropdown-link {
background-color: gray;
color: white;
display: block;
min-width: 100px;
padding: 1px 5px;
}

.dropdown-link:hover,
.dropdown-link--active {
background-color: darkgray;
}
`
}

declare global {
	interface HTMLElementTagNameMap {
		'yoummday-dropdown': YoummdayDropdown
	}
}
